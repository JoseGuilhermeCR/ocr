/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues   (joseguilhermebh0@protonmail.com)
 *                    Thiago Gonçalves (thiago.oliveira2001@gmail.com)
 *                    Pablo Rocha      (pabllosroc@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Arquivo para matéria de Processamento de Imagens do curso de Ciência da
 * Computação da Pontifícia Universidade Católica de Minas Gerais.
 *
 * José Guilherme de Castro Rodrigues
 * Thiago Oliveira Gonçalves
 * Pablo Rocha
 */
#ifndef OCR_HH
#define OCR_HH

#include <opencv2/core.hpp>

#include <array>
#include <vector>

class SVMClassifier;
class NeuralNetworkClassifier;

class OCR
{
public:
  static uint32_t constexpr MAX_SIZE_PER_DIMENSION = 28;
  static uint32_t constexpr PROJECTION_SIZE = MAX_SIZE_PER_DIMENSION * 2;

  OCR() = default;

  /**
   * @brief SetImage
   * Configura a imagem que o OCR usará.
   *
   * @param image Matriz representando imagem.
   */
  void SetImage(cv::Mat image) noexcept;

  /**
   * @brief ApplyOtsu
   * Aplica Otsu sobre a imagem.
   */
  void ApplyOtsu() noexcept;

  /**
   * @brief ApplyMedianFilter
   * Aplica um filtro de mediana sobre a imagem.
   * @param matrixSize Tamanho da matriz do filtro de mediana.
   */
  void ApplyMedianFilter(int32_t matrixSize) noexcept;

  /**
   * @brief DetectDigits
   * Detecta os dígitos da imagem.
   */
  void DetectDigits() noexcept;

  /**
   * @brief ApplyRotation
   * Realiza uma regressão linear com pontos específicos de cada dígito afim de
   * encontrar um ângulo e aplicar uma rotação para deixar a sequência mais
   * "reta".
   */
  void ApplyRotation() noexcept;

  /**
   * @brief BuildProjections
   * Constrói a projeção dos dígitos.
   */
  void BuildProjections() noexcept;

  /**
   * @brief Clear
   * Limpa as variáveis de controle do OCR: contornos, bounding boxes e
   * projeções.
   */
  void Clear() noexcept;

  /**
   * @brief ClassifyWithNN
   * Classifica as projeções usando uma rede neural.
   *
   * @param nnClassifier Rede Neural já configurada.
   * @param result Sequência respondida pela rede neural.
   * @param execTime Tempo de execução.
   * @return Sucesso, ou falha.
   */
  bool ClassifyWithNN(NeuralNetworkClassifier& nnClassifier,
                      std::vector<uint32_t>& result,
                      float& execTime) noexcept;

  /**
   * @brief ClassifyWithSVM
   * Classifica as projeções usando uma SVM.
   *
   * @param svmClassifier SVM já configurada.
   * @param result Sequência respondida pela SVM.
   * @param execTime Tempo de execução.
   * @return Sucesso, ou falha.
   */
  bool ClassifyWithSVM(SVMClassifier& svmClassifier,
                       std::vector<float>& result,
                       float& execTime) noexcept;

  /**
   * @brief GetImage
   * @return
   */
  cv::Mat const& GetImage() const noexcept { return m_image; }

  /**
   * @brief GetContours
   * @return
   */
  std::vector<std::vector<cv::Point>> const& GetContours() const noexcept
  {
    return m_contours;
  }

  /**
   * @brief GetDigitBoundingBoxes
   * @return
   */
  std::vector<cv::Rect> const& GetDigitBoundingBoxes() const noexcept
  {
    return m_digitBoundingBoxes;
  }

  /**
   * @brief GetProjections
   * @return
   */
  std::vector<std::array<float, PROJECTION_SIZE>> const& GetProjections()
    const noexcept
  {
    return m_projections;
  }

private:
  /**
   * @brief BuildProjectionDirectlyFromImage
   * A diferença para a BuildProjections() é que essa função
   * não depende de encontrar digitos na imagem. Ela só
   * criará a projeção da imagem passada e pronto. Caso a imagem
   * passada seja nula, a projeção de m_image será feita.
   *
   * @param image A imagem cuja projeção será criada, caso seja nullptr, a
   * projeção de m_image será criada.
   * @param shouldInvert Inverte a paleta de cinza da imagem.
   */
  void BuildProjectionDirectlyFromImage(cv::Mat* image = nullptr,
                                        bool shouldInvert = false) noexcept;

  cv::Mat m_image;
  std::vector<std::vector<cv::Point>> m_contours;
  std::vector<cv::Rect> m_digitBoundingBoxes;
  std::vector<std::array<float, PROJECTION_SIZE>> m_projections;
};

#endif // OCR_HH
