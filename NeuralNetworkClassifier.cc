/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues   (joseguilhermebh0@protonmail.com)
 *                    Thiago Gonçalves (thiago.oliveira2001@gmail.com)
 *                    Pablo Rocha      (pabllosroc@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Arquivo para matéria de Processamento de Imagens do curso de Ciência da
 * Computação da Pontifícia Universidade Católica de Minas Gerais.
 *
 * José Guilherme de Castro Rodrigues
 * Thiago Oliveira Gonçalves
 * Pablo Rocha
 */
#include "NeuralNetworkClassifier.hh"

#include <QDebug>

#include <chrono>
#include <thread>

NeuralNetworkClassifier::NeuralNetworkClassifier() noexcept
  : m_network(nullptr)
{
  m_network = cv::ml::ANN_MLP::create();

  // Configura topologia da rede.
  int32_t constexpr inputLayerSize = OCR::PROJECTION_SIZE;
  int32_t constexpr outputLayerSize = 10;
  std::array<int32_t, 4> constexpr layerSizes{ inputLayerSize,
                                               OCR::PROJECTION_SIZE * 2,
                                               OCR::PROJECTION_SIZE * 2,
                                               outputLayerSize };
  m_network->setLayerSizes(layerSizes);

  // Configura critério para terminação do algoritmo.
  m_network->setTermCriteria(cv::TermCriteria(
    cv::TermCriteria::MAX_ITER + cv::TermCriteria::EPS, 10'000'000, 0.000001));
}

void
NeuralNetworkClassifier::Train(
  std::vector<std::array<float, OCR::PROJECTION_SIZE>> const& trainData,
  std::vector<std::array<float, 10>> const& labels) noexcept
{
  if (!trainData.size() || !labels.size() ||
      labels.size() != trainData.size()) {
    return;
  }

  // Coloca todos as projeções que serão usadas no treinamento
  // em um grande vetor.
  std::vector<float> trainDataVec;
  trainDataVec.reserve(OCR::PROJECTION_SIZE * trainData.size());

  for (auto const& projection : trainData) {
    for (auto const& value : projection) {
      trainDataVec.push_back(value);
    }
  }

  // Agora, criamos uma matriz do OpenCV que terá cada projeção em uma linha.
  cv::Mat trainDataMat(static_cast<int32_t>(trainData.size()),
                       static_cast<int32_t>(OCR::PROJECTION_SIZE),
                       CV_32F,
                       reinterpret_cast<void*>(trainDataVec.data()));

  // Coloca todas as respostas em um grande vetor.
  std::vector<float> labelsDataVec;
  labelsDataVec.resize(labels.size() * 10);

  for (auto const& label : labels) {
    for (auto const& value : label) {
      labelsDataVec.push_back(value);
    }
  }

  // Agora, criamos uma matriz do OpenCV que terá cada reposta em uma linha.
  cv::Mat labelsMat(static_cast<int32_t>(labels.size()),
                    10,
                    CV_32F,
                    reinterpret_cast<void*>(labelsDataVec.data()));

  // Cria um thread que ficará printando uma mensagem a cada 30 segundos
  // para indicar execução do treino.
  std::atomic_bool trainingFinished = false;
  std::thread stillAliveThread([&trainingFinished]() {
    auto const start = std::chrono::steady_clock::now();
    auto printStart = start;
    while (!trainingFinished) {
      auto const now = std::chrono::steady_clock::now();

      auto const elapsedSinceLastPrint =
        std::chrono::duration<double, std::milli>(now - printStart).count();
      auto const elapsed =
        std::chrono::duration<double, std::milli>(now - start).count();

      if (elapsedSinceLastPrint > 30'000.0) {
        qCritical() << "NeuralNetworkClassifier::Train: Training... elapsed "
                    << elapsed / 1'000.0 << "s.";
        printStart = now;
      }
    }
  });

  // Cria dados de treinamento, especificando projeções,
  // cada projeção em uma linha e as respostas.
  cv::Ptr<cv::ml::TrainData> data = cv::ml::TrainData::create(
    trainDataMat, cv::ml::SampleTypes::ROW_SAMPLE, labelsMat);

  constexpr uint32_t TRAINING_EPOCHS = 8;
  bool result = false;

  // Treina o número de TRAINING_EPOCHS.
  for (uint32_t u = 0; u != TRAINING_EPOCHS; ++u) {
    result = m_network->train(
      data, cv::ml::ANN_MLP::NO_INPUT_SCALE | cv::ml::ANN_MLP::NO_OUTPUT_SCALE);
    qCritical() << u + 1 << " / " << TRAINING_EPOCHS << "epochs trained";
  }

  qCritical() << "NeuralNetworkClassifier::Train() result: " << result
              << "Is trained: " << m_network->isTrained();

  // Avisa para thread que o treinamento terminou e espera ele terminar a
  // sua execução.
  trainingFinished = true;
  stillAliveThread.join();
}

float
NeuralNetworkClassifier::Predict(
  std::vector<std::array<float, OCR::PROJECTION_SIZE>> const& predictData,
  cv::Mat& result,
  float& execTime) noexcept
{
  // Vamos colocar todos os dados em um grande vetor.
  std::vector<float> predictDataVec;
  predictDataVec.reserve(OCR::PROJECTION_SIZE * predictData.size());

  for (auto const& projection : predictData) {
    for (auto const& value : projection) {
      predictDataVec.push_back(value);
    }
  }

  // Agora, criamos uma matriz do OpenCV que terá cada projeção em uma linha.
  cv::Mat predictDataMat(static_cast<int32_t>(predictData.size()),
                         OCR::PROJECTION_SIZE,
                         CV_32F,
                         reinterpret_cast<void*>(predictDataVec.data()));

  auto const start = std::chrono::steady_clock::now();
  float const r = m_network->predict(predictDataMat, result);
  auto const end = std::chrono::steady_clock::now();

  execTime = std::chrono::duration<float>(end - start).count();
  return r;
}

bool
NeuralNetworkClassifier::LoadTrainedData(std::string const& path) noexcept
{
  try {
    m_network = cv::ml::ANN_MLP::load(path);
  } catch (...) {
    return false;
  }

  return true;
}

bool
NeuralNetworkClassifier::SaveTrainedData(std::string const& path)
{
  m_network->save(path);
  return true;
}
