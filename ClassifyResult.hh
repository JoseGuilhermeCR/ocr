/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues   (joseguilhermebh0@protonmail.com)
 *                    Thiago Gonçalves (thiago.oliveira2001@gmail.com)
 *                    Pablo Rocha      (pabllosroc@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Arquivo para matéria de Processamento de Imagens do curso de Ciência da
 * Computação da Pontifícia Universidade Católica de Minas Gerais.
 *
 * José Guilherme de Castro Rodrigues
 * Thiago Oliveira Gonçalves
 * Pablo Rocha
 */
#ifndef CLASSIFYRESULT_HH
#define CLASSIFYRESULT_HH

#include <QWidget>

#include <vector>

namespace Ui {
class ClassifyResult;
}

class ClassifyResult : public QWidget
{
  Q_OBJECT

public:
  explicit ClassifyResult(QWidget* parent = nullptr);
  ~ClassifyResult();

  /**
   * @brief SetSequence
   * Configura a sequência que será mostrada pela janela.
   *
   * @param sequence Sequência de inteiros.
   */
  void SetSequence(std::vector<uint32_t> const& sequence) noexcept;

  /**
   * @brief SetTime
   * Configura o tempo que será mostrado pela janela.
   *
   * @param time Tempo.
   */
  void SetTime(float time) noexcept;

private:
  Ui::ClassifyResult* ui;
};

#endif // CLASSIFYRESULT_HH
