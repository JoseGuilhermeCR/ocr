/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues   (joseguilhermebh0@protonmail.com)
 *                    Thiago Gonçalves (thiago.oliveira2001@gmail.com)
 *                    Pablo Rocha      (pabllosroc@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Arquivo para matéria de Processamento de Imagens do curso de Ciência da
 * Computação da Pontifícia Universidade Católica de Minas Gerais.
 *
 * José Guilherme de Castro Rodrigues
 * Thiago Oliveira Gonçalves
 * Pablo Rocha
 */
#ifndef TESTRESULT_HH
#define TESTRESULT_HH

#include <QTableWidget>
#include <QWidget>

#include <array>

namespace Ui {
class TestResult;
}

class TestResult : public QWidget
{
  Q_OBJECT

public:
  explicit TestResult(QWidget* parent = nullptr);
  ~TestResult();

  /**
   * @brief SetTotal
   * Configura o total que será mostrado pela janela.
   * @param total Total
   */
  void SetTotal(uint32_t total) noexcept;

  /**
   * @brief SetHits
   * Configura o acerto que será mostrado pela janela.
   * @param hits Acertos
   */
  void SetHits(uint32_t hits) noexcept;

  /**
   * @brief SetMisses
   * Configura o erro que será mostrado pela janela.
   * @param misses Erros
   */
  void SetMisses(uint32_t misses) noexcept;

  /**
   * @brief SetAccuracy
   * Configura a precisão que será mostrada pela janela.
   * @param accuracy Precisão
   */
  void SetAccuracy(float accuracy) noexcept;

  /**
   * @brief SetTime
   * Configura o tempo que será mostrado pela janela.
   * @param time Tempo
   */
  void SetTime(float time) noexcept;

  /**
   * @brief SetConfusionMatrix
   * Configura a matriz de confusão que será mostrada na janela.
   *
   * @param confusionMatrix Matriz de Confusão.
   */
  void SetConfusionMatrix(
    std::array<std::array<uint32_t, 10>, 10> const& confusionMatrix) noexcept;

private:
  Ui::TestResult* ui;
  std::vector<std::shared_ptr<QTableWidgetItem>> m_itemPointers;
};

#endif // TESTRESULT_HH
