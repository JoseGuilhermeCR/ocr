/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues   (joseguilhermebh0@protonmail.com)
 *                    Thiago Gonçalves (thiago.oliveira2001@gmail.com)
 *                    Pablo Rocha      (pabllosroc@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Arquivo para matéria de Processamento de Imagens do curso de Ciência da
 * Computação da Pontifícia Universidade Católica de Minas Gerais.
 *
 * José Guilherme de Castro Rodrigues
 * Thiago Oliveira Gonçalves
 * Pablo Rocha
 */
#include "MainWindow.hh"
#include "AboutDialog.hh"
#include "TestResult.hh"
#include "ui_MainWindow.h"

#include <QDebug>
#include <QMessageBox>
#include <QScrollBar>

#include <array>
#include <filesystem>
#include <vector>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

MainWindow::MainWindow(QWidget* parent) noexcept
  : QMainWindow(parent)
  , m_ui(new Ui::MainWindow)
  , m_scene()
  , m_ocr()
  , m_windowTitle("OCR")
{
  m_ui->setupUi(this);
  m_ui->m_graphicsView->setScene(&m_scene);

  m_scene.setBackgroundBrush(QBrush(QColor(0xf6, 0xf5, 0xf4)));

  setWindowTitle(m_windowTitle);

  // Conecta menu "File".
  connect(
    m_ui->m_actionOpen, &QAction::triggered, this, &MainWindow::OpenClicked);
  connect(m_ui->m_actionExit, &QAction::triggered, this, &MainWindow::close);

  // Conecta menu "Steps".
  connect(
    m_ui->m_actionApplyOtsu, &QAction::triggered, this, &MainWindow::ApplyOtsu);
  connect(m_ui->m_actionMedianFilter,
          &QAction::triggered,
          this,
          &MainWindow::MedianFilter);
  connect(m_ui->m_actionDetectDigits,
          &QAction::triggered,
          this,
          &MainWindow::DetectDigits);
  connect(m_ui->m_actionRotate, &QAction::triggered, this, &MainWindow::Rotate);
  connect(m_ui->m_actionBuildProjections,
          &QAction::triggered,
          this,
          &MainWindow::BuildProjections);
  connect(m_ui->m_actionClassifyDigits,
          &QAction::triggered,
          this,
          &MainWindow::ClassifyDigits);
  connect(
    m_ui->m_actionAllSteps, &QAction::triggered, this, &MainWindow::AllSteps);

  // Conecta menu "SVM".
  connect(m_ui->m_actionSVMTrain,
          &QAction::triggered,
          this,
          &MainWindow::SVMTrainClicked);
  connect(m_ui->m_actionLoadSVMTrainedData,
          &QAction::triggered,
          this,
          &MainWindow::SVMLoadClicked);
  connect(
    m_ui->m_actionSVMTest, &QAction::triggered, this, &MainWindow::SVMTest);

  // Conecta menu "Neural Network.
  connect(m_ui->m_actionNNTrain,
          &QAction::triggered,
          this,
          &MainWindow::NNTrainClicked);
  connect(m_ui->m_actionNNLoad,
          &QAction::triggered,
          this,
          &MainWindow::NNLoadClicked);
  connect(m_ui->m_actionNNTest, &QAction::triggered, this, &MainWindow::NNTest);

  // Conecta menu "Help".
  connect(
    m_ui->m_actionAbout, &QAction::triggered, this, &MainWindow::AboutClicked);
}

MainWindow::~MainWindow() noexcept
{
  delete m_ui;
}

void
MainWindow::UpdateImage() noexcept
{
  // Limpa a cena do Qt.
  m_scene.clear();

  // Cria uma imagem do Qt a partir de uma cv::Mat.
  auto const& image = m_ocr.GetImage();
  QImage const qimage(
    image.data, image.cols, image.rows, image.step, QImage::Format_Grayscale8);
  m_scene.addPixmap(QPixmap::fromImage(qimage));

  // Configura o tamanho da cena para o mesmo tamanho da imagem.
  m_scene.setSceneRect(0, 0, image.cols, image.rows);
}

void
MainWindow::DrawContour(QColor const& color) noexcept
{
  // Os contornos foram pegos de forma aproximada.
  // Ao invés de pegarmos todos os pontos deles, pegamos
  // só alguns, e fazemos linhas entre eles para representar
  // o contorno usando menos memória.
  auto const& contours = m_ocr.GetContours();
  for (auto const& contour : contours) {
    for (uint32_t u = 0; u < contour.size() - 1; u += 2) {
      // Desenha uma linha entre o ponto atual e o próximo ponto.
      m_scene.addLine(contour[u].x,
                      contour[u].y,
                      contour[u + 1].x,
                      contour[u + 1].y,
                      QPen{ color });
    }
  }
}

void
MainWindow::DrawBoundingBoxes(QColor const& color) noexcept
{
  auto const& digitBoundingBoxes = m_ocr.GetDigitBoundingBoxes();
  for (auto const& rect : digitBoundingBoxes) {
    // Calculamos as coordenadas para os quatro pontos da bounding box.
    std::array<cv::Point, 4> const points{
      cv::Point{ rect.x, rect.y },
      cv::Point{ rect.x + rect.width, rect.y },
      cv::Point{ rect.x + rect.width, rect.y + rect.height },
      cv::Point{ rect.x, rect.y + rect.height }
    };

    // Desenhamos linhas entre os pontos de forma a criar um retângulo.
    m_scene.addLine(
      points[0].x, points[0].y, points[1].x, points[1].y, QPen{ color });
    m_scene.addLine(
      points[1].x, points[1].y, points[2].x, points[2].y, QPen{ color });
    m_scene.addLine(
      points[2].x, points[2].y, points[3].x, points[3].y, QPen{ color });
    m_scene.addLine(
      points[3].x, points[3].y, points[0].x, points[0].y, QPen{ color });
  }
}

uint32_t
MainWindow::LoadDigitImages(
  std::array<std::vector<cv::Mat>, 10>& images) noexcept
{
  // Recupera o nome do diretório escolhido na janela de seleção de diretórios.
  QString const dirname =
    QFileDialog::getExistingDirectory(this, "OCR - Mnist Database Folder: ");

  uint32_t imageCount = 0;
  std::filesystem::path const mnistPath(dirname.toStdString());

  if (mnistPath.empty()) {
    return 0;
  }

  auto const start = std::chrono::steady_clock::now();
  // Para cada entrada no diretório que seja outro diretório:
  for (auto const& dirEntry : std::filesystem::directory_iterator(mnistPath)) {
    if (!dirEntry.is_directory()) {
      continue;
    }

#if defined(OCR_DEBUG_)
    qDebug() << "Taking a look at dirEntry: " << dirEntry.path();
#endif

    // Pegamos o nome do subdiretório.
    std::string const dirname = dirEntry.path().filename().string();

    // Verificamos que o nome só tem um dígito entre 0 e 9, ou seja,
    // é um diretório que contém imagens do dígito.
    if (dirname.size() == 1 && dirname.front() >= '0' &&
        dirname.front() <= '9') {
#if defined(OCR_DEBUG_)
      qDebug() << "Reading images from digit: " << dirname.front();
#endif

      // Colocamos as imagens na posição específica do arranjo de imagens
      // representada pelo dígito.
      uint8_t const digit = static_cast<uint8_t>(std::stoi(dirname));
      std::vector<cv::Mat>& digitImages = images[digit];

      // Carregamos a imagem de cada dígito dentro do diretório.
      for (auto const& digitDirEntry :
           std::filesystem::directory_iterator(dirEntry)) {
        digitImages.push_back(
          cv::imread(digitDirEntry.path().string(), cv::IMREAD_GRAYSCALE));
        ++imageCount;
      }
    }
  }
  auto const end = std::chrono::steady_clock::now();

  float const imageLoadTime = std::chrono::duration<float>(end - start).count();

  qCritical() << "ImageLoadTime: " << imageLoadTime;

  return imageCount;
}

void
MainWindow::ApplyOtsu() noexcept
{
  m_ocr.ApplyOtsu();

  UpdateImage();
  m_ui->m_actionMedianFilter->setEnabled(true);
  m_ui->m_actionDetectDigits->setEnabled(true);
}

void
MainWindow::MedianFilter() noexcept
{
  m_ocr.ApplyMedianFilter(3);

  UpdateImage();
}

void
MainWindow::DetectDigits() noexcept
{
  m_ocr.DetectDigits();

  DrawContour(QColor(255, 0, 0));
  DrawBoundingBoxes(QColor(0, 255, 0));

  m_ui->m_actionRotate->setEnabled(true);
}

void
MainWindow::Rotate() noexcept
{
  m_ocr.ApplyRotation();

  UpdateImage();

  // Agora que rotacionamos a imagem, precisamos detectar
  // os digitos novamente... Para pegar as caixas de cada
  // um já rotacionados.
  // Na verdade, esse é só um jeito mais fácil de fazer,
  // acaba sendo menos ótimo.
  DetectDigits();

  m_ui->m_actionBuildProjections->setEnabled(true);
}

void
MainWindow::BuildProjections() noexcept
{
  m_ocr.BuildProjections();

  QMessageBox::information(this, "OCR", "Projections built!");

  if (m_svmClassifier.IsTrained() || m_nnClassifier.IsTrained()) {
    m_ui->m_actionClassifyDigits->setEnabled(true);
  }
}

void
MainWindow::ClassifyDigits() noexcept
{
  std::vector<float> svmResult;
  float execTime = 0.0f;

  if (m_ocr.ClassifyWithSVM(m_svmClassifier, svmResult, execTime)) {
    QString info = "Execution Time: " + QString::number(execTime) + "s\n";
    info += "Sequence: ";

    for (float value : svmResult) {
      info += QString::number(static_cast<uint32_t>(value)) + ' ';
    }

    m_svmClassifyResult = std::make_unique<ClassifyResult>();
    m_svmClassifyResult->setWindowTitle("SVM - Classification Information");

    // Uma simples conversão para um vetor de uint32_t que é o que
    // a janela aceitará.
    std::vector<uint32_t> svmResultUint;
    for (float value : svmResult) {
      svmResultUint.push_back(static_cast<uint32_t>(value));
    }

    m_svmClassifyResult->SetSequence(svmResultUint);
    m_svmClassifyResult->SetTime(execTime);
    m_svmClassifyResult->show();
  }

  std::vector<uint32_t> nnResult;
  if (m_ocr.ClassifyWithNN(m_nnClassifier, nnResult, execTime)) {
    QString info = "Execution Time: " + QString::number(execTime) + "s\n";
    info += "Sequence: ";

    for (uint32_t value : nnResult) {
      info += QString::number(value) + ' ';
    }

    m_nnClassifyResult = std::make_unique<ClassifyResult>();
    m_nnClassifyResult->setWindowTitle("NN - Classification Information");
    m_nnClassifyResult->SetSequence(nnResult);
    m_nnClassifyResult->SetTime(execTime);
    m_nnClassifyResult->show();
  }
}

void
MainWindow::AllSteps() noexcept
{
  uint32_t constexpr DEFAULT_MEDIAN_FILTER_MATRIX_SIZE = 3;

  m_ocr.Clear();
  m_ocr.ApplyOtsu();
  m_ocr.ApplyMedianFilter(DEFAULT_MEDIAN_FILTER_MATRIX_SIZE);
  m_ocr.DetectDigits();
  m_ocr.ApplyRotation();
  m_ocr.DetectDigits();
  m_ocr.BuildProjections();

  UpdateImage();

  DrawContour(QColor(0, 0, 255));
  DrawBoundingBoxes(QColor(0, 255, 0));

  ClassifyDigits();
}

void
MainWindow::OpenClicked() noexcept
{
  QString const filename = QFileDialog::getOpenFileName(this);

  if (filename.isEmpty()) {
    return;
  }

  qDebug() << "Tring to open file at " << filename << '.';

  // Carrega imagem que será utilizada daqui para a frente.
  m_ocr.SetImage(cv::imread(filename.toStdString(), cv::IMREAD_GRAYSCALE));

  if (!m_ocr.GetImage().data) {
    QMessageBox::critical(this, "OCR", "Could not load image!");
    return;
  }

  // Configura as opções do menu.
  m_ui->m_actionApplyOtsu->setEnabled(true);
  m_ui->m_actionDetectDigits->setEnabled(false);
  m_ui->m_actionMedianFilter->setEnabled(false);
  m_ui->m_actionRotate->setEnabled(false);
  m_ui->m_actionBuildProjections->setEnabled(false);
  m_ui->m_actionClassifyDigits->setEnabled(false);
  m_ui->m_actionAllSteps->setEnabled(false);

  if (m_nnClassifier.IsTrained()) {
    m_ui->m_actionNNTest->setEnabled(true);
    m_ui->m_actionAllSteps->setEnabled(true);
  } else {
    m_ui->m_actionNNTest->setEnabled(false);
  }

  if (m_svmClassifier.IsTrained()) {
    m_ui->m_actionSVMTest->setEnabled(true);
    m_ui->m_actionAllSteps->setEnabled(true);
  } else {
    m_ui->m_actionSVMTest->setEnabled(false);
  }
  setWindowTitle(m_windowTitle + QString(" - Viewing ") +
                 filename.split('/').last());

  UpdateImage();
}

void
MainWindow::SVMTrainClicked() noexcept
{
  // Salva a imagem atual e limpa o OCR.
  cv::Mat const oldImage = m_ocr.GetImage();
  m_ocr.Clear();

  std::array<std::vector<cv::Mat>, 10> digits;
  uint32_t const imageCount = LoadDigitImages(digits);

  if (!imageCount) {
    return;
  }

  auto start = std::chrono::steady_clock::now();

  std::vector<std::array<float, OCR::PROJECTION_SIZE>> projections;
  std::vector<int32_t> labels;
  projections.reserve(imageCount);
  labels.reserve(imageCount);

  // Monta as projeções das imagens que serão treinadas e as respostas
  // de cada uma delas.
  for (uint8_t u = 0; u != digits.size(); ++u) {
#if defined(OCR_DEBUG_)
    qDebug() << "Digit: " << static_cast<int32_t>(u);
#endif
    for (auto const& image : digits[u]) {
      m_ocr.SetImage(image);
      m_ocr.ApplyOtsu();
      m_ocr.DetectDigits();
      m_ocr.BuildProjections();

      // As imagens de treino só tem uma projeção... a do dígito de treino,
      // por isso pegamos só .front().
      if (m_ocr.GetProjections().size() == 1) {
        projections.push_back(m_ocr.GetProjections().front());
        labels.push_back(u);
      }
    }
  }

  auto end = std::chrono::steady_clock::now();
  float const projectionsBuildTime =
    std::chrono::duration<float>(end - start).count();
  qCritical() << "Projections build time: " << projectionsBuildTime;

  qCritical() << "Starting training of " << projections.size()
              << " projections.";
  start = std::chrono::steady_clock::now();

  m_svmClassifier.Train(projections, labels);

  end = std::chrono::steady_clock::now();
  float const trainTime = std::chrono::duration<float>(end - start).count();

  qCritical() << "Train time: " << trainTime;
  m_svmClassifier.SaveTrainedData("trainedSVM.xml");

  // Agora volta com imagem que estava na interface.
  m_ocr.SetImage(oldImage);

  // Habilita opções de classificação dependendo
  // do estado da imagem que voltou para interface.
  if (m_ocr.GetImage().data) {
    m_ui->m_actionAllSteps->setEnabled(true);
    if (m_ui->m_actionBuildProjections->isEnabled()) {
      m_ui->m_actionClassifyDigits->setEnabled(true);
    }
  }

  m_ui->m_actionSVMTest->setEnabled(true);
}

void
MainWindow::SVMLoadClicked() noexcept
{
  QString const filename =
    QFileDialog::getOpenFileName(this, "Choose an SVM trained data file: ");

  if (filename.isEmpty()) {
    return;
  }

  if (m_svmClassifier.LoadTrainedData(filename.toStdString())) {
    m_ui->m_actionSVMTest->setEnabled(true);

    if (m_ocr.GetImage().data) {
      m_ui->m_actionAllSteps->setEnabled(true);
      if (m_ui->m_actionBuildProjections->isEnabled()) {
        m_ui->m_actionClassifyDigits->setEnabled(true);
      }
    }

    QMessageBox::information(this, "OCR", "Loaded trained data!");
    return;
  }

  QMessageBox::critical(this, "OCR", "Could not load trained data!");
}

void
MainWindow::SVMTest() noexcept
{
  if (!m_svmClassifier.IsTrained()) {
    QMessageBox::critical(this, "OCR", "SVM isn't trained yet!");
    return;
  }

  // Salva a imagem atual e limpa o OCR.
  cv::Mat const oldImage = m_ocr.GetImage();
  m_ocr.Clear();

  std::array<std::vector<cv::Mat>, 10> digits;
  uint32_t const imageCount = LoadDigitImages(digits);

  if (!imageCount) {
    return;
  }

  std::vector<std::array<float, OCR::PROJECTION_SIZE>> projections;
  std::vector<int32_t> labels;
  projections.reserve(imageCount);
  labels.reserve(imageCount);

  // Monta as projeções das imagens que serão treinadas e as respostas
  // de cada uma delas.
  for (uint8_t u = 0; u != digits.size(); ++u) {
#if defined(OCR_DEBUG_)
    qDebug() << "Digit: " << static_cast<int32_t>(u);
#endif
    for (auto const& image : digits[u]) {
      m_ocr.SetImage(image);
      m_ocr.ApplyOtsu();
      m_ocr.DetectDigits();
      m_ocr.BuildProjections();

      // As imagens de teste também só devem ter um dígito, por isso só uma
      // projeção será criada e então acessamos com .front().
      if (m_ocr.GetProjections().size() == 1) {
        projections.push_back(m_ocr.GetProjections().front());
        labels.push_back(u);
      }
    }
  }

  std::vector<float> result;
  float execTime;

  m_svmClassifier.Predict(projections, result, execTime);

  if (labels.size() != result.size()) {
    qCritical()
      << "Number of labels is different from number of elements in result.";
    return;
  }

  uint32_t const total = static_cast<uint32_t>(labels.size());
  uint32_t miss = 0;
  uint32_t hit = 0;

  // Cria matriz de confusão e a zera.
  std::array<std::array<uint32_t, 10>, 10> confusionMatrix;
  for (uint32_t row = 0; row != confusionMatrix.size(); ++row) {
    for (uint32_t col = 0; col != confusionMatrix[0].size(); ++col) {
      confusionMatrix[row][col] = 0;
    }
  }

  // Checamos a reposta de cada projeção com a obitda pela SVM.
  for (uint32_t u = 0; u != labels.size(); ++u) {
    if (labels[u] == static_cast<int32_t>(result[u])) {
      ++hit;
    } else {
      ++miss;
    }

    // Aumenta o índice na matriz de confusão.
    // Linha da resposta correta, coluna da resposta dada.
    ++confusionMatrix[labels[u]][static_cast<uint32_t>(result[u])];
  }

  // Cria janela para exibição das estatísticas.
  m_svmTestResult = std::make_unique<TestResult>();
  m_svmTestResult->setWindowTitle("SVM - Test Result");
  m_svmTestResult->SetTotal(total);
  m_svmTestResult->SetHits(hit);
  m_svmTestResult->SetMisses(miss);
  m_svmTestResult->SetAccuracy(static_cast<float>(hit) / total * 100.0f);
  m_svmTestResult->SetTime(execTime);
  m_svmTestResult->SetConfusionMatrix(confusionMatrix);
  m_svmTestResult->show();

  m_ocr.SetImage(oldImage);
}

void
MainWindow::NNTrainClicked() noexcept
{
  // Salva a imagem atual e limpa o OCR.
  cv::Mat const oldImage = m_ocr.GetImage();
  m_ocr.Clear();

  std::array<std::vector<cv::Mat>, 10> digits;
  uint32_t const imageCount = LoadDigitImages(digits);

  if (!imageCount) {
    return;
  }

  auto start = std::chrono::steady_clock::now();

  std::vector<std::array<float, OCR::PROJECTION_SIZE>> projections;
  std::vector<std::array<float, 10>> labels;
  projections.reserve(imageCount);
  labels.reserve(imageCount);

  // Monta as projeções e os vetores de resposta.
  for (uint8_t u = 0; u != digits.size(); ++u) {
    // A resposta do dígito U é indicada pela posição U
    // do vetor de respostas sendo 1.0f.
    std::array<float, 10> output;
    for (uint32_t v = 0; v != output.size(); ++v) {
      if (v == u) {
        output[v] = 1.0f;
      } else {
        output[v] = 0.0f;
      }
    }

    qDebug() << "Digit: " << static_cast<int32_t>(u);
    for (auto const& image : digits[u]) {
      m_ocr.SetImage(image);
      m_ocr.ApplyOtsu();
      m_ocr.DetectDigits();
      m_ocr.BuildProjections();

      // As imagens de treino só tem uma projeção... a do dígito de treino,
      // por isso pegamos só .front().
      if (m_ocr.GetProjections().size() == 1) {
        projections.push_back(m_ocr.GetProjections().front());
        labels.push_back(output);
      }
    }
  }

  auto end = std::chrono::steady_clock::now();
  float const projectionsBuildTime =
    std::chrono::duration<float>(end - start).count();
  qCritical() << "Projections build time: " << projectionsBuildTime;

  qCritical() << "Starting training of " << projections.size()
              << " projections.";
  start = std::chrono::steady_clock::now();

  m_nnClassifier.Train(projections, labels);

  end = std::chrono::steady_clock::now();
  float const trainTime = std::chrono::duration<float>(end - start).count();

  qCritical() << "Train time: " << trainTime;
  m_nnClassifier.SaveTrainedData("trainedNN.xml");

  // Volta com imagem para interface.
  m_ocr.SetImage(oldImage);

  if (m_ocr.GetImage().data) {
    m_ui->m_actionAllSteps->setEnabled(true);
    if (m_ui->m_actionBuildProjections->isEnabled()) {
      m_ui->m_actionClassifyDigits->setEnabled(true);
    }
  }

  m_ui->m_actionNNTest->setEnabled(true);
}

void
MainWindow::NNLoadClicked() noexcept
{
  QString const filename = QFileDialog::getOpenFileName(
    this, "Choose an Neural Network trained data file: ");

  if (filename.isEmpty()) {
    return;
  }

  if (m_nnClassifier.LoadTrainedData(filename.toStdString())) {
    m_ui->m_actionNNTest->setEnabled(true);

    if (m_ocr.GetImage().data) {
      m_ui->m_actionAllSteps->setEnabled(true);
      if (m_ui->m_actionBuildProjections->isEnabled()) {
        m_ui->m_actionClassifyDigits->setEnabled(true);
      }
    }

    QMessageBox::information(this, "OCR", "Loaded trained data!");
    return;
  }

  QMessageBox::critical(this, "OCR", "Could not load trained data!");
}

void
MainWindow::NNTest() noexcept
{
  if (!m_nnClassifier.IsTrained()) {
    QMessageBox::critical(this, "OCR", "Neural Network isn't trained yet!");
    return;
  }

  cv::Mat const oldImage = m_ocr.GetImage();
  m_ocr.Clear();

  std::array<std::vector<cv::Mat>, 10> digits;
  uint32_t const imageCount = LoadDigitImages(digits);

  if (!imageCount) {
    return;
  }

  std::vector<std::array<float, OCR::PROJECTION_SIZE>> projections;
  std::vector<std::array<float, 10>> labels;
  projections.reserve(imageCount);
  labels.reserve(imageCount);

  // Monta as projeções e os vetores de resposta.
  for (uint8_t u = 0; u != digits.size(); ++u) {
    // A resposta do dígito U é indicada pela posição U
    // do vetor de respostas sendo 1.0f.
    std::array<float, 10> output;
    for (uint32_t v = 0; v != output.size(); ++v) {
      if (v == u) {
        output[v] = 1.0f;
      } else {
        output[v] = 0.0f;
      }
    }
#if defined(OCR_DEBUG_)
    qDebug() << "Digit: " << static_cast<int32_t>(u);
#endif
    for (auto const& image : digits[u]) {
      m_ocr.SetImage(image);
      m_ocr.ApplyOtsu();
      m_ocr.DetectDigits();
      m_ocr.BuildProjections();

      // As imagens de teste também só devem ter um dígito, por isso só uma
      // projeção será criada e então acessamos com .front().
      if (m_ocr.GetProjections().size() == 1) {
        projections.push_back(m_ocr.GetProjections().front());
        labels.push_back(output);
      }
    }
  }

  cv::Mat result;
  float execTime;
  m_nnClassifier.Predict(projections, result, execTime);

  if (result.rows != labels.size()) {
    qCritical()
      << "Number of labels is different from number of elements in result.";
    return;
  }

  uint32_t const total = static_cast<uint32_t>(labels.size());
  uint32_t miss = 0;
  uint32_t hit = 0;

  // Cria matriz de confusão e a zera.
  std::array<std::array<uint32_t, 10>, 10> confusionMatrix;
  for (uint32_t row = 0; row != confusionMatrix.size(); ++row) {
    for (uint32_t col = 0; col != confusionMatrix[0].size(); ++col) {
      confusionMatrix[row][col] = 0;
    }
  }

  // Checamos a reposta de cada projeção com a obtida pela SVM.
  for (uint32_t u = 0; u != labels.size(); ++u) {
    // Pega posição do vetor que tem valor 1.0f.
    uint8_t labelDigitAnswer = 0;
    while (labelDigitAnswer != labels[u].size() &&
           labels[u][labelDigitAnswer] < 1.0f) {
      ++labelDigitAnswer;
    }

    // Pega posição na respota da rede que tem a maior probabilidade.
    uint8_t resultDigit = 0;
    for (uint8_t v = 1; v != result.cols; ++v) {
      float const currentMax = result.at<float>(u, resultDigit);
      float const value = result.at<float>(u, v);
      if (value > currentMax) {
        resultDigit = v;
      }
    }

    if (resultDigit == labelDigitAnswer) {
      ++hit;
    } else {
      ++miss;
    }

    // Aumenta o índice na matriz de confusão.
    // Linha da resposta correta, coluna da resposta dada.
    ++confusionMatrix[labelDigitAnswer][resultDigit];
  }

  // Cria janela para exibição das estatísticas.
  m_nnTestResult = std::make_unique<TestResult>();
  m_nnTestResult->setWindowTitle("NN - Test Result");
  m_nnTestResult->SetTotal(total);
  m_nnTestResult->SetHits(hit);
  m_nnTestResult->SetMisses(miss);
  m_nnTestResult->SetAccuracy(static_cast<float>(hit) / total * 100.0f);
  m_nnTestResult->SetTime(execTime);
  m_nnTestResult->SetConfusionMatrix(confusionMatrix);
  m_nnTestResult->show();

  m_ocr.SetImage(oldImage);
}

void
MainWindow::AboutClicked() noexcept
{
  std::unique_ptr<AboutDialog> aboutDialog = std::make_unique<AboutDialog>();
  aboutDialog->setWindowTitle("OCR - About");
  aboutDialog->exec();
}
