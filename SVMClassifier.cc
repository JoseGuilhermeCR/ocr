/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues   (joseguilhermebh0@protonmail.com)
 *                    Thiago Gonçalves (thiago.oliveira2001@gmail.com)
 *                    Pablo Rocha      (pabllosroc@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Arquivo para matéria de Processamento de Imagens do curso de Ciência da
 * Computação da Pontifícia Universidade Católica de Minas Gerais.
 *
 * José Guilherme de Castro Rodrigues
 * Thiago Oliveira Gonçalves
 * Pablo Rocha
 */
#include "SVMClassifier.hh"
#include "OCR.hh"

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/ml.hpp>

#include <QDebug>

#include <chrono>
#include <thread>

SVMClassifier::SVMClassifier() noexcept
  : m_svm(nullptr)
{
  m_svm = cv::ml::SVM::create();
  m_svm->setType(cv::ml::SVM::C_SVC);
  m_svm->setKernel(cv::ml::SVM::LINEAR);
  m_svm->setC(100);
  m_svm->setTermCriteria(
    cv::TermCriteria(cv::TermCriteria::MAX_ITER, 10'000'00, 0.000001));
}

void
SVMClassifier::Train(
  std::vector<std::array<float, OCR::PROJECTION_SIZE>> const& trainData,
  std::vector<int32_t> const& labels) noexcept
{
  // Garante que não teremos exceção gerada pelo OpenCV.
  // O número de linhas nos dados de treinamento deve
  // ser igual o número de labels.
  if (!trainData.size() || !labels.size() ||
      labels.size() != trainData.size()) {
    return;
  }

  // Vamos colocar todos os dados em um grande vetor.
  std::vector<float> trainDataVec;
  trainDataVec.reserve(OCR::PROJECTION_SIZE * trainData.size());

  for (auto const& projection : trainData) {
    for (auto const& value : projection) {
      trainDataVec.push_back(value);
    }
  }

  // Agora, criamos uma matriz do OpenCV que terá cada projeção em uma linha.
  cv::Mat trainDataMat(static_cast<int32_t>(trainData.size()),
                       static_cast<int32_t>(OCR::PROJECTION_SIZE),
                       CV_32F,
                       reinterpret_cast<void*>(trainDataVec.data()));

  cv::Mat labelsMat(labels);
  // Cria um thread que ficará printando uma mensagem a cada 30 segundos
  // para indicar execução do treino.
  std::atomic_bool trainingFinished = false;
  std::thread stillAliveThread([&trainingFinished]() {
    auto const start = std::chrono::steady_clock::now();
    auto printStart = start;
    while (!trainingFinished) {
      auto const now = std::chrono::steady_clock::now();

      auto const elapsedSinceLastPrint =
        std::chrono::duration<double, std::milli>(now - printStart).count();
      auto const elapsed =
        std::chrono::duration<double, std::milli>(now - start).count();

      if (elapsedSinceLastPrint > 30'000.0) {
        qCritical() << "SVMClassifier::Train: Training... elapsed "
                    << elapsed / 1'000.0 << "s.";
        printStart = now;
      }
    }
  });

  bool const result =
    m_svm->train(trainDataMat, cv::ml::SampleTypes::ROW_SAMPLE, labelsMat);

  qCritical() << "SVMClassifier::Train() result: " << result
              << "Is trained: " << m_svm->isTrained();

  // Avisa para thread que o treinamento terminou e espera ele terminar a
  // sua execução.
  trainingFinished = true;
  stillAliveThread.join();
}

float
SVMClassifier::Predict(
  std::vector<std::array<float, OCR::PROJECTION_SIZE>> const& predictData,
  std::vector<float>& result,
  float& execTime) noexcept
{
  // Vamos colocar todos os dados em um grande vetor.
  std::vector<float> predictDataVec;
  predictDataVec.reserve(OCR::PROJECTION_SIZE * predictData.size());

  for (auto const& projection : predictData) {
    for (auto const& value : projection) {
      predictDataVec.push_back(value);
    }
  }

  cv::Mat predictDataMat(static_cast<int32_t>(predictData.size()),
                         OCR::PROJECTION_SIZE,
                         CV_32F,
                         reinterpret_cast<void*>(predictDataVec.data()));

  auto const start = std::chrono::steady_clock::now();
  float const r = m_svm->predict(predictDataMat, result);
  auto const end = std::chrono::steady_clock::now();

  execTime = std::chrono::duration<float>(end - start).count();
  return r;
}

bool
SVMClassifier::LoadTrainedData(std::string const& path) noexcept
{
  try {
    m_svm = cv::ml::SVM::load(path);
  } catch (...) {
    return false;
  }

  return true;
}

bool
SVMClassifier::SaveTrainedData(std::string const& path)
{
  m_svm->save(path);
  return true;
}
