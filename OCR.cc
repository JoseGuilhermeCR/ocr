/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues   (joseguilhermebh0@protonmail.com)
 *                    Thiago Gonçalves (thiago.oliveira2001@gmail.com)
 *                    Pablo Rocha      (pabllosroc@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Arquivo para matéria de Processamento de Imagens do curso de Ciência da
 * Computação da Pontifícia Universidade Católica de Minas Gerais.
 *
 * José Guilherme de Castro Rodrigues
 * Thiago Oliveira Gonçalves
 * Pablo Rocha
 */
#include "OCR.hh"

#include "NeuralNetworkClassifier.hh"
#include "SVMClassifier.hh"

#include <QDebug>

#include <algorithm>
#include <array>
#include <vector>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

void
OCR::ApplyOtsu() noexcept
{
  if (!m_image.data) {
    return;
  }

  // Lineariza a imagem utilizando detecção de threshold por algoritmo de Otsu.
  cv::threshold(
    m_image, m_image, 0.0, 255.0, cv::THRESH_BINARY | cv::THRESH_OTSU);
  // Pegamos a média da imagem no único canal, no caso o cinza.
  double const imageMean = cv::mean(m_image)[0];

  constexpr uint8_t TOO_DARK = 128;
  if (imageMean < TOO_DARK) {
    // Se a média da imagem está abaixo de TOO_DARK, consideraremos que a imagem
    // tem o fundo preto após a linearização. Nesse caso, inverteremos a paleta
    // de cinza. A ideia é que o fundo seja claro e os dígitos escuros.
    cv::bitwise_not(m_image, m_image);
  }
}

void
OCR::ApplyMedianFilter(int32_t matrixSize) noexcept
{
  if (!m_image.data) {
    return;
  }

  if (matrixSize % 2 == 0 || matrixSize <= 1) {
    return;
  }

  cv::medianBlur(m_image, m_image, matrixSize);
}

void
OCR::DetectDigits() noexcept
{
  if (!m_image.data) {
    return;
  }

  m_digitBoundingBoxes.clear();
  m_contours.clear();

  std::vector<cv::Vec4i> hierarchy;
  cv::findContours(m_image,
                   m_contours,
                   hierarchy,
                   cv::RETR_CCOMP,
                   cv::CHAIN_APPROX_SIMPLE,
                   cv::Point());

  constexpr int32_t MIN_SIZE = 5;
  for (uint32_t u = 0; u != m_contours.size(); ++u) {
    // Se o contorno não tiver filho e tiver pai na hierarquia.
    if (hierarchy[u][3] > -1 && hierarchy[u][2] == -1) {
      cv::Rect const rect = cv::boundingRect(m_contours[u]);
      // Se for muito pequeno, provavelmente é só um ruído.
      // Ignore.
      if (rect.width > MIN_SIZE && rect.height > MIN_SIZE) {
        m_digitBoundingBoxes.push_back(rect);
      }
    }
  }
}

void
OCR::ApplyRotation() noexcept
{
  if (!m_image.data) {
    return;
  }

  // Coletamos os centros de todos os retângulos dos dígitos.
  std::vector<cv::Point2f> centers(m_digitBoundingBoxes.size());
  for (uint32_t u = 0; u != centers.size(); ++u) {
    auto const& boundingBox = m_digitBoundingBoxes[u];
    centers[u] = cv::Point2f{ boundingBox.x + boundingBox.width / 2.0f,
                              boundingBox.y + boundingBox.height / 2.0f };
  }

  if (centers.empty()) {
    return;
  }

  // Faz uma regressão linear e salva o resultado em line.
  cv::Vec4f line;
  cv::fitLine(centers, line, cv::DIST_L2, 0.0, 0.01, 0.01);

#if defined(OCR_DEBUG_)
  qDebug() << "Normalized Vector: (" << line[0] << ", " << line[1]
           << "). Line Point: (" << line[2] << ", " << line[3] << ")";
#endif

  // Pegamos o slope da linha da regressão utilizando o vetor normalizado.
  float const slope = line[1] / line[0];
  // Para pegar o ângulo do slope, basta utilizar arco tangente.
  float const angle = atanf(slope) * (180.0 / M_PI);

#if defined(OCR_DEBUG_)
  qDebug() << "Angle: " << angle;
#endif

  // Agora, basta criar a matriz de rotação e realizar a transformaçã sobre a
  // imagem.
  cv::Mat const rotMat = cv::getRotationMatrix2D(
    cv::Point2f(m_image.cols / 2, m_image.rows / 2), angle, 1.0);
  cv::warpAffine(
    m_image, m_image, rotMat, cv::Size(m_image.cols, m_image.rows));
}

void
OCR::BuildProjections() noexcept
{
  // Ordene as bounding boxes por coordenada x para seguir a ordem da sequência
  // na imagem.
  std::sort(m_digitBoundingBoxes.begin(),
            m_digitBoundingBoxes.end(),
            [](cv::Rect& a, cv::Rect& b) { return a.x < b.x; });

  for (auto const& box : m_digitBoundingBoxes) {
    cv::Mat roi = m_image(box);
    BuildProjectionDirectlyFromImage(&roi);
  }
}

void
OCR::SetImage(cv::Mat image) noexcept
{
  Clear();
  m_image = image;
}

void
OCR::Clear() noexcept
{
  m_contours.clear();
  m_digitBoundingBoxes.clear();
  m_projections.clear();
}

bool
OCR::ClassifyWithNN(NeuralNetworkClassifier& nnClassifier,
                    std::vector<uint32_t>& result,
                    float& execTime) noexcept
{
  if (!nnClassifier.IsTrained() || m_projections.empty()) {
    return false;
  }

  cv::Mat resultMat;
  nnClassifier.Predict(m_projections, resultMat, execTime);

  // Cada linha é a resposta de uma projeção.
  for (uint32_t row = 0; row != resultMat.rows; ++row) {
    // Pegamos o índice da coluna que tem a maior probabilidade.
    // Esse é o dígito que foi gerado pela rede como resultado.
    uint32_t max = 0;
    for (uint32_t col = 1; col != resultMat.cols; ++col) {
      float const currentMax = resultMat.at<float>(row, max);
      float const value = resultMat.at<float>(row, col);
      if (value > currentMax) {
        max = col;
      }
    }

    result.push_back(max);
  }

  return true;
}

bool
OCR::ClassifyWithSVM(SVMClassifier& svmClassifier,
                     std::vector<float>& result,
                     float& execTime) noexcept
{
  if (!svmClassifier.IsTrained() || m_projections.empty()) {
    return false;
  }

  svmClassifier.Predict(m_projections, result, execTime);
  return true;
}

void
OCR::BuildProjectionDirectlyFromImage(cv::Mat* image,
                                      bool shouldInvert) noexcept
{
  // Se image for nullptr, fazemos a projeção da imagem salva em m_image.
  if (!image) {
    image = &m_image;
  }

  if (shouldInvert) {
    // Inverte a paleta.
    cv::bitwise_not(*image, *image);
  }

  int32_t colFlag = 0;
  int32_t rowFlag = 0;

  // Deixa a imagem com as dimensões necessárias, utilizando o melhor método de
  // interpolação.
  if (image->cols > MAX_SIZE_PER_DIMENSION) {
    colFlag = cv::INTER_AREA;

  } else {
    colFlag = cv::INTER_CUBIC;
  }

  if (image->rows > MAX_SIZE_PER_DIMENSION) {
    rowFlag = cv::INTER_AREA;
  } else {
    rowFlag = cv::INTER_CUBIC;
  }

  // Dá um resize na imagem em cada dimensão de uma vez para usar o melhor
  // método para cada uma delas.
  cv::resize(*image,
             *image,
             cv::Size(MAX_SIZE_PER_DIMENSION, image->rows),
             0.0,
             0.0,
             colFlag);
  cv::resize(*image,
             *image,
             cv::Size(image->cols, MAX_SIZE_PER_DIMENSION),
             0.0,
             0.0,
             rowFlag);

  // Reduz a matriz para uma única coluna.
  cv::Mat horizontalOutput;
  cv::reduce(*image, horizontalOutput, 1, cv::REDUCE_SUM, CV_32F);

  // Faz a transposta para deixar essa matriz na forma N x 1.
  cv::transpose(horizontalOutput, horizontalOutput);

  // Reduz a matriz para uma única linha.
  cv::Mat verticalOutput;
  cv::reduce(*image, verticalOutput, 0, cv::REDUCE_SUM, CV_32F);

  // Concatena as projeções, temos uma matriz PROJECTION_SIZE x 1.
  cv::Mat resultingProjection;
  cv::hconcat(horizontalOutput, verticalOutput, resultingProjection);

  // Normalizamos essa projeção. Todos valores estão entre 0 e 1.
  cv::normalize(resultingProjection,
                resultingProjection,
                1.0,
                0.0,
                cv::NORM_MINMAX,
                CV_32F);

  // Atenção: Nesse ponto, cv::Mat vira std::array<float, PROJECTION_SIZE>.
  m_projections.push_back(resultingProjection);
}
