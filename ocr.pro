QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++20

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += OCR.cc \
           AboutDialog.cc \
           ClassifyResult.cc \
           MainWindow.cc \
           NeuralNetworkClassifier.cc \
           SVMClassifier.cc \
           TestResult.cc \
           main.cc

HEADERS += OCR.hh \
           AboutDialog.hh \
           ClassifyResult.hh \
           MainWindow.hh \
           NeuralNetworkClassifier.hh \
           SVMClassifier.hh \
           TestResult.hh

FORMS += MainWindow.ui \
    AboutDialog.ui \
    ClassifyResult.ui \
    TestResult.ui

CONFIG(debug, debug|release) {
    INCLUDEPATH += C:\opencv\debug\include
    LIBS += -LC:\opencv\debug\lib \
            -lopencv_core454d \
            -lopencv_highgui454d \
            -lopencv_imgcodecs454d \
            -lopencv_imgproc454d \
            -lopencv_features2d454d \
            -lopencv_calib3d454d \
            -lopencv_ml454d
    DEFINES += OCR_DEBUG_
}

CONFIG(release, debug|release) {
    INCLUDEPATH += C:\opencv\release\include
    LIBS += -LC:\opencv\release\lib \
        -lopencv_core454 \
        -lopencv_highgui454 \
        -lopencv_imgcodecs454 \
        -lopencv_imgproc454 \
        -lopencv_features2d454 \
        -lopencv_calib3d454 \
        -lopencv_ml454
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
