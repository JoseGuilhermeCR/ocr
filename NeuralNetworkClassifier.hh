/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues   (joseguilhermebh0@protonmail.com)
 *                    Thiago Gonçalves (thiago.oliveira2001@gmail.com)
 *                    Pablo Rocha      (pabllosroc@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Arquivo para matéria de Processamento de Imagens do curso de Ciência da
 * Computação da Pontifícia Universidade Católica de Minas Gerais.
 *
 * José Guilherme de Castro Rodrigues
 * Thiago Oliveira Gonçalves
 * Pablo Rocha
 */
#ifndef NEURALNETWORKCLASSIFIER_HH
#define NEURALNETWORKCLASSIFIER_HH

#include "OCR.hh"

#include <opencv2/ml.hpp>

class NeuralNetworkClassifier
{
public:
  NeuralNetworkClassifier() noexcept;

  /**
   * @brief Train
   * Treina a rede neural.
   *
   * @param trainData Projeções que serão usadas no treinamento.
   * @param labels Respostas das projeções passadas.
   */
  void Train(
    std::vector<std::array<float, OCR::PROJECTION_SIZE>> const& trainData,
    std::vector<std::array<float, 10>> const& labels) noexcept;

  /**
   * @brief Predict
   * Prediz as projeções.
   *
   * @param predictData Projeções que devem ser preditas.
   *
   * @param result Matriz onde o resultado é guardado.
   * Cada linha guarda a resposta de uma projeção e cada coluna indica a
   * probabilidade do dígito referente ao número da coluna ser o predito.
   *
   * @param execTime Tempo de execução.
   *
   * @return
   */
  float Predict(
    std::vector<std::array<float, OCR::PROJECTION_SIZE>> const& predictData,
    cv::Mat& result,
    float& execTime) noexcept;

  /**
   * @brief SaveTrainedData
   * @param path Caminho onde dados serão salvos.
   * @return Sucesso ou falha.
   */
  bool SaveTrainedData(std::string const& path);

  /**
   * @brief LoadTrainedData
   * @param path Caminho de onde dados serão carregados.
   * @return Sucesso ou falha.
   */
  bool LoadTrainedData(std::string const& path) noexcept;

  /**
   * @brief IsTrained
   * Diz a a rede foi ou não treinada.
   *
   * @return
   */
  bool IsTrained() const noexcept { return m_network->isTrained(); }

private:
  cv::Ptr<cv::ml::ANN_MLP> m_network;
};

#endif // NEURALNETWORKCLASSIFIER_HH
