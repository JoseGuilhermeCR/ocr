/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues   (joseguilhermebh0@protonmail.com)
 *                    Thiago Gonçalves (thiago.oliveira2001@gmail.com)
 *                    Pablo Rocha      (pabllosroc@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Arquivo para matéria de Processamento de Imagens do curso de Ciência da
 * Computação da Pontifícia Universidade Católica de Minas Gerais.
 *
 * José Guilherme de Castro Rodrigues
 * Thiago Oliveira Gonçalves
 * Pablo Rocha
 */
#ifndef SVMCLASSIFIER_HH
#define SVMCLASSIFIER_HH

#include "OCR.hh"

#include <array>
#include <memory>
#include <opencv2/ml.hpp>
#include <vector>

class SVMClassifier
{
public:
  SVMClassifier() noexcept;

  /**
   * @brief Train
   * Treina um modelo de SVM.
   *
   * @param trainData Projeções que serão usadas no treino.
   * @param labels As respostas de cada projeção.
   */
  void Train(
    std::vector<std::array<float, OCR::PROJECTION_SIZE>> const& trainData,
    std::vector<int32_t> const& labels) noexcept;

  /**
   * @brief Predict
   * Tenta predizer a classe de um conjunto de projeções.
   *
   * @param predictData Projeções.
   * @param result Resposta de cada uma delas.
   * @param execTime Tempo de execução para a classificação.
   *
   * @return Retorno de cv::ml::SVM::predict().
   */
  float Predict(
    std::vector<std::array<float, OCR::PROJECTION_SIZE>> const& predictData,
    std::vector<float>& result,
    float& execTime) noexcept;

  /**
   * @brief LoadTrainedData
   * Carrega dados de uma SVM treinada de um caminho.
   *
   * @param path Caminho para o arquivo.
   *
   * @return bool
   */
  bool LoadTrainedData(std::string const& path) noexcept;

  /**
   * @brief SaveTrainedData
   * Salva os dados de uma SVM treinada em um caminho.
   *
   * @param path Caminho para o arquivo.
   *
   * @return bool
   */
  bool SaveTrainedData(std::string const& path);

  /**
   * @brief IsTrained
   * @return
   */
  bool IsTrained() const noexcept { return m_svm->isTrained(); }

private:
  cv::Ptr<cv::ml::SVM> m_svm;
};

#endif // SVMCLASSIFIER_HH
