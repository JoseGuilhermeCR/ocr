/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues   (joseguilhermebh0@protonmail.com)
 *                    Thiago Gonçalves (thiago.oliveira2001@gmail.com)
 *                    Pablo Rocha      (pabllosroc@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Arquivo para matéria de Processamento de Imagens do curso de Ciência da
 * Computação da Pontifícia Universidade Católica de Minas Gerais.
 *
 * José Guilherme de Castro Rodrigues
 * Thiago Oliveira Gonçalves
 * Pablo Rocha
 */
#include "TestResult.hh"
#include "ui_TestResult.h"

TestResult::TestResult(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::TestResult)
{
  ui->setupUi(this);

  QStringList const headers{ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
  ui->m_confusionMatrix->setVerticalHeaderLabels(headers);
  ui->m_confusionMatrix->setHorizontalHeaderLabels(headers);
}

TestResult::~TestResult()
{
  delete ui;
}

void
TestResult::SetTotal(uint32_t total) noexcept
{
  ui->m_total->setText(QString::number(total));
}

void
TestResult::SetHits(uint32_t hits) noexcept
{
  ui->m_hit->setText(QString::number(hits));
}

void
TestResult::SetMisses(uint32_t misses) noexcept
{
  ui->m_miss->setText(QString::number(misses));
}

void
TestResult::SetAccuracy(float accuracy) noexcept
{
  ui->m_accuracy->setText(QString::number(accuracy) + "%");
}

void
TestResult::SetConfusionMatrix(
  std::array<std::array<uint32_t, 10>, 10> const& confusionMatrix) noexcept
{
  ui->m_confusionMatrix->setSortingEnabled(false);

  ui->m_confusionMatrix->setRowCount(10);
  ui->m_confusionMatrix->setColumnCount(10);

  for (uint32_t row = 0; row != confusionMatrix.size(); ++row) {
    for (uint32_t col = 0; col != confusionMatrix[0].size(); ++col) {
      m_itemPointers.push_back(std::make_shared<QTableWidgetItem>(
        QString::number(confusionMatrix[row][col])));
      ui->m_confusionMatrix->setItem(row, col, m_itemPointers.back().get());
    }
  }
}

void
TestResult::SetTime(float time) noexcept
{
    ui->m_time->setText(QString::number(time) + "s");
}
