/* MIT License
 *
 * Copyright (c) 2021 José Rodrigues   (joseguilhermebh0@protonmail.com)
 *                    Thiago Gonçalves (thiago.oliveira2001@gmail.com)
 *                    Pablo Rocha      (pabllosroc@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * Arquivo para matéria de Processamento de Imagens do curso de Ciência da
 * Computação da Pontifícia Universidade Católica de Minas Gerais.
 *
 * José Guilherme de Castro Rodrigues
 * Thiago Oliveira Gonçalves
 * Pablo Rocha
 */
#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

#include <QFileDialog>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMainWindow>
#include <QMessageBox>
#include <QString>

#include <opencv2/core.hpp>

#include <memory>
#include <vector>

#include "ClassifyResult.hh"
#include "NeuralNetworkClassifier.hh"
#include "OCR.hh"
#include "SVMClassifier.hh"
#include "TestResult.hh"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget* parent = nullptr) noexcept;
  ~MainWindow() noexcept;

private:
  /**
   * @brief UpdateImage
   * Atualiza a imagem da interface para ser condizente com a interface dentro
   * de m_ocr.
   */
  void UpdateImage() noexcept;

  /**
   * @brief DrawContour
   * Desenha os contornos dos dígitos na interface.
   *
   * @param color Cor dos contornos.
   */
  void DrawContour(QColor const& color = QColor(255, 0, 0)) noexcept;

  /**
   * @brief DrawBoundingBoxes
   * Desenha as bounding boxes dos dígitos na interface.
   *
   * @param color Cor das bounding boxes.
   */
  void DrawBoundingBoxes(QColor const& color = QColor(0, 0, 0)) noexcept;

  /**
   * @brief LoadDigitImages
   * Carrega imagens de cada dígito de 0-9 para memória.
   *
   * @param images Imagens carregadas.
   *
   * @return Número de imagens carregadas.
   */
  uint32_t LoadDigitImages(
    std::array<std::vector<cv::Mat>, 10>& images) noexcept;

  Ui::MainWindow* m_ui;

  QGraphicsScene m_scene;

  OCR m_ocr;
  SVMClassifier m_svmClassifier;
  NeuralNetworkClassifier m_nnClassifier;

  QString m_windowTitle;

  std::unique_ptr<TestResult> m_svmTestResult;
  std::unique_ptr<TestResult> m_nnTestResult;
  std::unique_ptr<ClassifyResult> m_svmClassifyResult;
  std::unique_ptr<ClassifyResult> m_nnClassifyResult;
public slots:
  /**
   * @brief OpenClicked
   * Cuida da abertura de uma imagem para análise.
   */
  void OpenClicked() noexcept;

  /**
   * @brief ApplyOtsu
   * Aplica o Otsu e atualiza a representação na interface.
   */
  void ApplyOtsu() noexcept;

  /**
   * @brief MedianFilter
   * Aplica o filtro de mediana e atualiza a representação na interface.
   */
  void MedianFilter() noexcept;

  /**
   * @brief DetectDigits
   * Detecta os dígitos e atualiza a representação na interface.
   */
  void DetectDigits() noexcept;

  /**
   * @brief Rotate
   * Rotaciona a imagem e atualiza a representação na interface.
   */
  void Rotate() noexcept;

  /**
   * @brief BuildProjections
   * Constrói a projeção dos dígitos apresentados na interface.
   */
  void BuildProjections() noexcept;

  /**
   * @brief ClassifyDigits
   * Classifica os dígitos usando os classificadores já treinados.
   */
  void ClassifyDigits() noexcept;

  /**
   * @brief AllSteps
   * Executa todos os passos de uma vez só para tentar identificar os dígitos da
   * imagem.
   */
  void AllSteps() noexcept;

  /**
   * @brief SVMTrainClicked
   * Começa processo de treino da SVM.
   */
  void SVMTrainClicked() noexcept;

  /**
   * @brief SVMLoadClicked
   * Carrega arquivo de treino da SVM.
   */
  void SVMLoadClicked() noexcept;

  /**
   * @brief SVMTest
   * Testa a SVM que foi treinada.
   */
  void SVMTest() noexcept;

  /**
   * @brief NNTrainClicked
   * Começa o processo de treino da Rede Neural.
   */
  void NNTrainClicked() noexcept;

  /**
   * @brief NNLoadClicked
   * Carrega arquivo de treino da Rede Neural.
   */
  void NNLoadClicked() noexcept;

  /**
   * @brief NNTest
   * Testa a Rede Neural que foi treinada.
   */
  void NNTest() noexcept;

  /**
   * @brief AboutClicked
   * Mostra tela de "Sobre".
   */
  void AboutClicked() noexcept;
};
#endif // MAINWINDOW_HH
